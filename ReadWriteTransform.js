const Readable = require("stream").Readable;
const Writable = require("stream").Writable;
const Transform = require("stream").Transform;

class CReadable extends Readable {
	constructor(options) {
		super(options);
	}
	_read() {
		this.push((Math.random()*100).toString());
	}
};

class CWritable extends Writable {
	constructor(options) {
		super(options);
	}
	_write(chunk, encoding, done) {
		console.log("_write");
		console.log(chunk);
		done();
	}
};


class CTransform extends Transform {
	constructor(options) {
		super(options);
	}
	_transform(chunk, encoding, callback) {
		setInterval(()=> {
			this.push((chunk*(Math.random()*100)).toString());
			callback();
		}, 1000)
	}
};

class MyTransform extends Transform {
	constructor(options) {
		super(options);
		this.crypto = require("crypto");
		this.hash = this.crypto.createHash("md5");
	}
	_transform(chunk, encoding, callback) {
		let hex = this.hash.update(chunk).digest("hex");
		console.log(hex);
		this.push(hex);
		callback();
	}
}

module.exports = {
	CTransform,
	CReadable,
	CWritable,
	MyTransform
}