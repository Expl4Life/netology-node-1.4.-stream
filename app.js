"use strict";
// ----------------------------------------
const MyTransform = require("./ReadWriteTransform").MyTransform;
// ----------------------------------------
const crypto = require("crypto");
const hash = crypto.createHash("md5");
// ----------------------------------------
const fs = require("fs");
// ----------------------------------------
const myReadStream = fs.createReadStream(__dirname + "/FORD-MyLife.txt", "utf8");
myReadStream.on("readable", () => {
	let data = myReadStream.read();
	if (data) {
		hash.update(data);
	} else {
		let hashSumm = hash.digest();
		let Readable = require("stream").Readable;
		let newReadStream = new Readable();
		newReadStream.push(hashSumm);
		newReadStream.push(null);
		// ----------------------------------------
		newReadStream.pipe(process.stdout);
		// ----------------------------------------
		const myWriteHashStream = fs.createWriteStream(__dirname + "/hashFord.txt");
		newReadStream.pipe(new MyTransform()).pipe(myWriteHashStream);
		console.log(hashSumm);
	}
});
// ----------------------------------------
// Final TASK
// ----------------------------------------
const CReadable = require("./ReadWriteTransform").CReadable;
const CWritable = require("./ReadWriteTransform").CWritable;
const CTransform = require("./ReadWriteTransform").CTransform;
// ----------------------------------------
const myRead = new CReadable();
const myWrite = new CWritable();
const myTransform = new CTransform();
myRead.pipe(myTransform).pipe(myWrite);
